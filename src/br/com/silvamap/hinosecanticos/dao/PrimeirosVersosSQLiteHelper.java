package br.com.silvamap.hinosecanticos.dao;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import br.com.silvamap.hinosecanticos.entidade.PrimeirosVersos;

public class PrimeirosVersosSQLiteHelper extends GenericSQLiteHelper {

	public PrimeirosVersosSQLiteHelper(Context context) {
		super(context);
	}

	public void insert(PrimeirosVersos primerosVersos) {
		SQLiteDatabase db = getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(PrimeirosVersos.ID, primerosVersos.getId());
		values.put(PrimeirosVersos.ID_HINO, primerosVersos.getIdHino());
		values.put(PrimeirosVersos.VERSOS, primerosVersos.getVersos());
		db.insert(PrimeirosVersos.TABLE_NAME, null, values);
		db.close();
	}
	
	public List<PrimeirosVersos> recuperarTodos() {
		List<PrimeirosVersos> retorno = new ArrayList<PrimeirosVersos>();
		SQLiteDatabase db = getReadableDatabase();
		Cursor cursor = db.query(PrimeirosVersos.TABLE_NAME, PrimeirosVersos.COLUMNS, null, null, null, null, null);
		if (cursor.moveToFirst()) {
			do {
				PrimeirosVersos primeirosVersos = new PrimeirosVersos();
				primeirosVersos.setId(cursor.getInt(0));
				primeirosVersos.setIdHino(cursor.getInt(1));
				primeirosVersos.setVersos(cursor.getString(2));
				retorno.add(primeirosVersos);
			} while (cursor.moveToNext());
		}
		cursor.close();
		db.close();
		return retorno;
	}

	@Override
	public String getTableName() {
		return PrimeirosVersos.TABLE_NAME;
	}
	
}
