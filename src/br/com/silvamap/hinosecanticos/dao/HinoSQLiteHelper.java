package br.com.silvamap.hinosecanticos.dao;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import br.com.silvamap.hinosecanticos.entidade.Hino;

@SuppressLint({ "DefaultLocale", "NewApi" })
public class HinoSQLiteHelper extends GenericSQLiteHelper {

	public HinoSQLiteHelper(Context context) {
		super(context);
	}

	public void insert(Hino hino) {
		SQLiteDatabase db = getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(Hino.ID, hino.getId());
		values.put(Hino.NOME, hino.getNome());
		db.insert(Hino.TABLE_NAME, null, values);
		db.close();
	}

	public Hino recuperarPorId(Integer id) {
		SQLiteDatabase db = getWritableDatabase();
		Cursor cursor = db.rawQuery("select * from hino where id = " + id, null);
		if (cursor.moveToFirst()) {
			do {
				Hino hino = new Hino();
				hino.setId(cursor.getString(0));
				hino.setNome(cursor.getString(1));
				return hino;
			} while (cursor.moveToNext());
		}
		cursor.close();
		db.close();
		return null;
	}

	public List<Hino> recuperarTodos() {
		List<Hino> retorno = new ArrayList<Hino>();
		SQLiteDatabase db = getReadableDatabase();
		Cursor cursor = db.query(Hino.TABLE_NAME, Hino.COLUMNS, null, null, null, null, null);
		if (cursor.moveToFirst()) {
			do {
				Hino hino = new Hino();
				hino.setId(cursor.getString(0));
				hino.setNome(cursor.getString(1));
				retorno.add(hino);
			} while (cursor.moveToNext());
		}
		cursor.close();
		db.close();
		return retorno;
	}

	@SuppressLint("DefaultLocale")
	public List<Hino> recuperarPorNome(String filtro) {
		List<Hino> retorno = new ArrayList<Hino>();
		SQLiteDatabase db = getReadableDatabase();
		Cursor cursor = db.rawQuery("select * from hino " + "where cast(id as text) like '%" + filtro.toLowerCase()
				+ "%' or " + "lower(nome) like '%" + filtro.toLowerCase() + "%' order by id", null);
		if (cursor.moveToFirst()) {
			do {
				Hino hino = new Hino();
				hino.setId(cursor.getString(0));
				hino.setNome(cursor.getString(1));
				retorno.add(hino);
			} while (cursor.moveToNext());
		}
		cursor.close();
		db.close();
		return retorno;
	}

	@SuppressLint("DefaultLocale")
	public List<Hino> recuperarPorFiltro(String filtro) {
		List<Hino> retorno = new ArrayList<Hino>();
		SQLiteDatabase db = getReadableDatabase();
		StringBuilder sql = new StringBuilder();
		sql.append("select  h.* from 'hino' h ");
		sql.append("left join 'letra' l on h.id = l.id_hino ");
		if (filtro != null && !filtro.isEmpty()) {
			sql.append("where cast(h.id as text) like '%" + filtro.toLowerCase() + "%' or ");
			sql.append("lower(h.nome) like '%" + filtro.toLowerCase() + "%' or ");
			sql.append("lower(l.letra) like '%" + filtro.toLowerCase() + "%'");
		}
		Cursor cursor = db.rawQuery(sql.toString(), null);
		if (cursor.moveToFirst()) {
			do {
				Hino hino = new Hino();
				hino.setId(cursor.getString(0));
				hino.setNome(cursor.getString(1));
				retorno.add(hino);
			} while (cursor.moveToNext());
		}
		cursor.close();
		db.close();
		return retorno;
	}


	public ArrayList<Hino> recuperarTodosFavoritos() {
		ArrayList<Hino> retorno = new ArrayList<Hino>();
		SQLiteDatabase db = getReadableDatabase();
		Cursor cursor = db.rawQuery(
				"select h.id, h.nome from hino as h, favorito as f where h.id = f.id_hino order by h.id", null);
		if (cursor.moveToFirst()) {
			do {
				Hino hino = new Hino();
				hino.setId(cursor.getString(0));
				hino.setNome(cursor.getString(1));
				retorno.add(hino);
			} while (cursor.moveToNext());
		} else {
			cursor = db.rawQuery(
					"select h.id, h.nome from hino as h, favorito as f where h.id = f.id_hino order by h.id", null);
			if (cursor.moveToFirst()) {
				do {
					Hino hino = new Hino();
					hino.setId(cursor.getString(0));
					hino.setNome(cursor.getString(1));
					retorno.add(hino);
				} while (cursor.moveToNext());
			}
		}
		cursor.close();
		db.close();
		return retorno;
	}

	@Override
	public Integer getCount() {
		SQLiteDatabase db = getWritableDatabase();
		Cursor cursor = db.rawQuery("select count(id) from " + getTableName(), null);
		if (cursor.moveToFirst()) {
			do {
				return cursor.getInt(0);
			} while (cursor.moveToNext());
		}
		cursor.close();
		db.close();
		return null;
	}

	@Override
	public String getTableName() {
		return Hino.TABLE_NAME;
	}
}
