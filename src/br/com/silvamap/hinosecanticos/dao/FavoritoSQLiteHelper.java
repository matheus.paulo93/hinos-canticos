package br.com.silvamap.hinosecanticos.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import br.com.silvamap.hinosecanticos.entidade.Favorito;

public class FavoritoSQLiteHelper extends GenericSQLiteHelper {

	public FavoritoSQLiteHelper(Context context) {
		super(context);
	}
	
	public void insert(Favorito favorito) {
		SQLiteDatabase db = getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(Favorito.ID, favorito.getId());
		values.put(Favorito.ID_HINO, favorito.getIdHino());
		db.insert(Favorito.TABLE_NAME, null, values);
		db.close();
	}
	
	public void delete(String id) {
		SQLiteDatabase db = getWritableDatabase();
		String whereClause = Favorito.ID_HINO + " = ?";
		String[] whereArgs = { id };
		db.delete(Favorito.TABLE_NAME, whereClause, whereArgs);
	}
	
	public boolean hinoFavorito(String id) {
		SQLiteDatabase db = getWritableDatabase();
		boolean retorno = false;
		Cursor cursor = db.rawQuery("select 1 from favorito where id_hino = " + id, null);
		if (cursor.moveToFirst()) {
			do {
				retorno =  true;
			} while (cursor.moveToNext());
		}
		cursor.close();
		db.close();
		return retorno;
	}

	@Override
	public String getTableName() {
		return Favorito.TABLE_NAME;
	}

}
