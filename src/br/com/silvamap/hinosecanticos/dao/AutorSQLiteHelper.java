package br.com.silvamap.hinosecanticos.dao;

import br.com.silvamap.hinosecanticos.entidade.Autor;
import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

public class AutorSQLiteHelper extends GenericSQLiteHelper {

	public AutorSQLiteHelper(Context context) {
		super(context);
	}
	
	public void insert(Autor autor) {
		SQLiteDatabase db = getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(Autor.ID, autor.getId());
		values.put(Autor.ID_HINO, autor.getIdHino());
		values.put(Autor.NOME_AUTOR, autor.getNomeAutor());
		db.insert(Autor.TABLE_NAME, null, values);
	}

	@Override
	public String getTableName() {
		return Autor.TABLE_NAME;
	}

}
