package br.com.silvamap.hinosecanticos.dao;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import br.com.silvamap.hinosecanticos.entidade.Autor;
import br.com.silvamap.hinosecanticos.entidade.Favorito;
import br.com.silvamap.hinosecanticos.entidade.Hino;
import br.com.silvamap.hinosecanticos.entidade.Letra;

public abstract class GenericSQLiteHelper extends SQLiteOpenHelper {

	private static int DATABASE_VERSION = 11;

	public GenericSQLiteHelper(Context context) {
		super(context, "hcco", null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {

		db.execSQL("create table if not exists " + Hino.TABLE_NAME + " (" + Hino.ID + " integer not null primary key,"
				+ Hino.NOME + " varchar(255))");

		db.execSQL("create table if not exists " + Autor.TABLE_NAME + " (" + Autor.ID
				+ " integer not null primary key autoincrement, " + Autor.ID_HINO + " integer not null, "
				+ Autor.NOME_AUTOR + " varchar(255))");

		db.execSQL("create table if not exists " + Letra.TABLE_NAME + " (" + Letra.ID
				+ " integer not null primary key autoincrement, " + Letra.ID_HINO + " integer not null, " + Letra.LETRA
				+ " varchar(10000))");

		db.execSQL("create table if not exists " + Favorito.TABLE_NAME + " (" + Favorito.ID
				+ " integer not null primary key autoincrement, " + Favorito.ID_HINO + " integer not null)");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		if (tabelaExiste(db, Hino.TABLE_NAME)) {
			db.execSQL("drop table if exists " + Hino.TABLE_NAME);
		}
		if (tabelaExiste(db, Autor.TABLE_NAME)) {
			db.execSQL("drop table if exists " + Autor.TABLE_NAME);
		}
		if (tabelaExiste(db, Letra.TABLE_NAME)) {
			db.execSQL("drop table if exists " + Letra.TABLE_NAME);
		}
		onCreate(db);
	}

	public Integer getCount() {
		SQLiteDatabase db = getWritableDatabase();
		Cursor cursor = db.rawQuery("select count(id) from " + getTableName(), null);
		if (cursor.moveToFirst()) {
			do {
				return cursor.getInt(0);
			} while (cursor.moveToNext());
		}
		cursor.close();
		db.close();
		return null;
	}

	public boolean tabelaExiste(SQLiteDatabase db, String nomeTabela) {
		Cursor cursor = db
				.rawQuery("select count() from sqlite_master where type='table' and name = '" + nomeTabela + "'", null);
		Integer count = 0;
		if (cursor.moveToFirst()) {
			do {
				count = cursor.getInt(0);
			} while (cursor.moveToNext());
		}
		cursor.close();
		return count == 1;
	}

	public abstract String getTableName();

}
