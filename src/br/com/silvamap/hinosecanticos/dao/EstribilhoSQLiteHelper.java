package br.com.silvamap.hinosecanticos.dao;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import br.com.silvamap.hinosecanticos.entidade.Estribilho;

public class EstribilhoSQLiteHelper extends GenericSQLiteHelper {

	public EstribilhoSQLiteHelper(Context context) {
		super(context);
	}
	
	public void insert(Estribilho estribilho) {
		SQLiteDatabase db = getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(Estribilho.ID, estribilho.getId());
		values.put(Estribilho.ID_HINO, estribilho.getIdHino());
		values.put(Estribilho.ESTRIBILHO, estribilho.getEstribilho());
		db.insert(Estribilho.TABLE_NAME, null, values);
		db.close();
	}
	
	public List<Estribilho> recuperarTodos() {
		List<Estribilho> retorno = new ArrayList<Estribilho>();
		SQLiteDatabase db = getReadableDatabase();
		Cursor cursor = db.query(Estribilho.TABLE_NAME, Estribilho.COLUMNS, null, null, null, null, null);
		if (cursor.moveToFirst()) {
			do {
				Estribilho estribilho = new Estribilho();
				estribilho.setId(cursor.getInt(0));
				estribilho.setIdHino(cursor.getInt(1));
				estribilho.setEstribilho(cursor.getString(2));
				retorno.add(estribilho);
			} while (cursor.moveToNext());
		}
		cursor.close();
		db.close();
		return retorno;
	}

	@Override
	public String getTableName() {
		return Estribilho.TABLE_NAME;
	}

}
