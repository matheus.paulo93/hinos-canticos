package br.com.silvamap.hinosecanticos.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import br.com.silvamap.hinosecanticos.entidade.Letra;

public class LetraSQLiteHelper extends GenericSQLiteHelper {

	public LetraSQLiteHelper(Context context) {
		super(context);
	}
	
	public Long insert(Letra letra) {
		SQLiteDatabase db = getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(Letra.ID, letra.getId());
		values.put(Letra.ID_HINO, letra.getIdHino());
		values.put(Letra.LETRA, letra.getLetra());
		long retorno = db.insert(Letra.TABLE_NAME, null, values);
		db.close();
		return retorno;
	}
	
	public Letra recuperarPorHino(String idHino) {
		Letra letra = new Letra();
		SQLiteDatabase db = getReadableDatabase();
		Cursor cursor = db.rawQuery("select * from letra where id_hino = " + idHino, null);
		if (cursor.moveToFirst()) {
			do {
				letra.setId(cursor.getInt(0));
				letra.setIdHino(cursor.getInt(1));
				letra.setLetra(cursor.getString(2));
			} while (cursor.moveToNext());
		}
		cursor.close();
		db.close();
		return letra;
	}

	@Override
	public String getTableName() {
		return Letra.TABLE_NAME;
	}

}
