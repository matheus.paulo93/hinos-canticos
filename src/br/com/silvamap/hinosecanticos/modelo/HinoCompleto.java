package br.com.silvamap.hinosecanticos.modelo;

public class HinoCompleto {

	private String id;
	private String nome;
	private String primeroVerso;
	private String estribilho;
	private String nomeAutor;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getPrimeroVerso() {
		return primeroVerso;
	}

	public void setPrimeroVerso(String primeroVerso) {
		this.primeroVerso = primeroVerso;
	}

	public String getEstribilho() {
		return estribilho;
	}

	public void setEstribilho(String estribilho) {
		this.estribilho = estribilho;
	}

	public String getNomeAutor() {
		return nomeAutor;
	}

	public void setNomeAutor(String nomeAutor) {
		this.nomeAutor = nomeAutor;
	}
	
}
