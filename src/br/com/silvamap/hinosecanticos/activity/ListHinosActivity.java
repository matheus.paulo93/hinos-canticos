package br.com.silvamap.hinosecanticos.activity;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ace.hinoscanticos.R;

import android.annotation.SuppressLint;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.SearchView.OnQueryTextListener;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import br.com.silvamap.hinosecanticos.dao.HinoSQLiteHelper;
import br.com.silvamap.hinosecanticos.entidade.Hino;
import br.com.silvamap.hinosecanticos.util.AppRater;

@SuppressLint("NewApi")
public class ListHinosActivity extends ActionBarActivity implements OnItemClickListener, OnQueryTextListener {

	private List<Map<String, Object>> hinos;
	private int inoSelecionado;
	private String[] from = { "id", "nome"};
	private int[] to = { R.id.numeroHino, R.id.nomeHino};
	private String[] fromDrawerMenu = { "icone", "nome" };
	private int[] toDrawermenu = { R.id.icone_menu_drawer, R.id.itemMenuDrawer };
	private SearchView searchView;
	private ListView listViewHinos;
	private DrawerLayout mDrawerLayout;
	private ListView mDrawerList;
	private ActionBarDrawerToggle mDrawerToggle;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_list_hinos_froyo);
		getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.color.beige));
		listViewHinos = (ListView) findViewById(R.id.list_hinos_froyo);
		listViewHinos.setAdapter(new SimpleAdapter(this, listarHinos(null), R.layout.layout_list_hinos, from, to));
		listViewHinos.setOnItemClickListener(this);
		listViewHinos.setTextFilterEnabled(true);

		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout2);
		mDrawerList = (ListView) findViewById(R.id.left_drawer2);
		mDrawerList.setAdapter(new SimpleAdapter(this, recuperarItensMenuDrawer(), R.layout.drawer_list_item,
				fromDrawerMenu, toDrawermenu));
		mDrawerList.setOnItemClickListener(new DrawerItemClickListener());
		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.drawable.ic_drawer, R.string.drawer_open,
				R.string.drawer_close);

		mDrawerLayout.setDrawerListener(mDrawerToggle);

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);
		AppRater.app_launched(this);
//		AppRater.showRateDialog(this, null);
	}

	@SuppressWarnings("rawtypes")
	@Override
	public void onItemClick(AdapterView<?> adapter, View view, int index, long arg3) {
		HashMap hino = (HashMap) adapter.getAdapter().getItem(index);
		try {
			Field field;
			field = R.raw.class.getDeclaredField("hino" + hino.get("id"));
			field.setAccessible(true);
			inoSelecionado = (Integer) field.get(new R.raw());
			Intent intent = new Intent(this, HinoActivity.class);
			Bundle bundle = new Bundle();
			bundle.putInt("inoSelecionado", inoSelecionado);
			bundle.putString("idHino", hino.get("id").toString());
			intent.putExtras(bundle);
			startActivity(intent);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_main_froyo, menu);
		SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
		MenuItem menuItem = (MenuItem) menu.findItem(R.id.menu_item_pesquisar);
		searchView = (SearchView) MenuItemCompat.getActionView(menuItem);
		searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
		searchView.setOnQueryTextListener(this);

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onQueryTextChange(String newText) {
		HinoSQLiteHelper db = new HinoSQLiteHelper(this);
		if (TextUtils.isEmpty(newText)) {
			listViewHinos.clearTextFilter();
			listViewHinos.setAdapter(new SimpleAdapter(this, listarHinos(null), R.layout.layout_list_hinos, from, to));
		} else {
			listViewHinos.setAdapter(
					new SimpleAdapter(this, listarHinos(newText.toString()), R.layout.layout_list_hinos, from, to));
		}
		db.close();
		return true;
	}

	@Override
	public boolean onQueryTextSubmit(String query) {
		return false;
	}

	public List<Map<String, Object>> listarHinos(String nomeFiltro) {
		hinos = new ArrayList<Map<String, Object>>();
		HinoSQLiteHelper db = new HinoSQLiteHelper(this);
		List<Hino> listaHino = new ArrayList<Hino>();
		listaHino = db.recuperarPorFiltro(nomeFiltro);
		for (Hino item : listaHino) {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("id", item.getId().toString());
			map.put("nome", item.getNome());
			hinos.add(map);
		}
		db.close();
		return hinos;
	}

	public List<Hino> arrayHinos() {
		HinoSQLiteHelper db = new HinoSQLiteHelper(this);
		return db.recuperarTodos();
	}

	@Override
	public void onBackPressed() {
		if (searchView != null && !searchView.isIconified()) {
			searchView.setIconified(true);
			listViewHinos.setAdapter(new SimpleAdapter(this, listarHinos(null), R.layout.layout_list_hinos, from, to));
		} else {
			super.onBackPressed();
		}
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		mDrawerToggle.syncState();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * Swaps fragments in the main content view
	 */
	private void selectItem(int position) {
		switch (position) {
		case 0:
			startActivity(new Intent(this, PesquisaActivity.class));
			break;
		case 1:
			startActivity(new Intent(this, FavoritosActivity.class));
			break;
		case 2:
			shareApp();
			break;
		case 3:
			startActivity(new Intent(this, SobreActivity.class));
			break;
		case 4:
			rate();
			break;
		case 5:
			finish();
			break;
		default:
			break;
		}
		mDrawerLayout.closeDrawer(mDrawerList);
	}

	@SuppressWarnings("rawtypes")
	private class DrawerItemClickListener implements ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView parent, View view, int position, long id) {
			selectItem(position);
		}
	}

	public List<Map<String, Object>> recuperarItensMenuDrawer() {
		List<Map<String, Object>> listMapa = new ArrayList<Map<String, Object>>();
		Map<String, Object> item = new HashMap<String, Object>();
		item.put("nome", "Pesquisar");
		item.put("icone", R.drawable.ic_action_search);
		listMapa.add(item);
		item = new HashMap<String, Object>();
		item.put("nome", "Favoritos");
		item.put("icone", R.drawable.ic_action_favorite_yellow);
		listMapa.add(item);
		item = new HashMap<String, Object>();
		item.put("nome", "Compartilhar");
		item.put("icone", R.drawable.ic_action_share);
		listMapa.add(item);
		item = new HashMap<String, Object>();
		item.put("nome", "Sobre");
		item.put("icone", R.drawable.ic_action_about);
		listMapa.add(item);
		item = new HashMap<String, Object>();
		item.put("nome", "Avalie-nos");
		item.put("icone", R.drawable.ic_action_rate);
		listMapa.add(item);
		item = new HashMap<String, Object>();
		item.put("nome", "Fechar");
		item.put("icone", R.drawable.ic_action_close_app);
		listMapa.add(item);
		return listMapa;
	}

	public void shareApp() {
		String playStoreLink = "https://play.google.com/store/apps/details?id=" + getPackageName();
		String yourShareText = "Confira o aplicativo \"Hinos e C�nticos\" " + playStoreLink;
		Intent i = new Intent(android.content.Intent.ACTION_SEND);
		i.setType("text/plain");
		i.putExtra(android.content.Intent.EXTRA_TEXT, yourShareText);
		startActivity(Intent.createChooser(i, "Compartilhar via"));
	}

	public void rate() {
		Uri uri = Uri.parse("market://details?id=" + getPackageName());
		Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
		startActivity(goToMarket);
	}
	
}
