package br.com.silvamap.hinosecanticos.activity;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ace.hinoscanticos.R;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import br.com.silvamap.hinosecanticos.entidade.Hino;

public class ListHinosFiltro extends ActionBarActivity implements OnItemClickListener {

	private List<Map<String, Object>> hinos;
	private int inoSelecionado;
	private String[] from = { "id", "nome", "text_complemento" };
	private int[] to = { R.id.numeroHinoFiltro, R.id.nomeHinoFiltro, R.id.text_complemento };
	private ListView listViewHinos;
	private String textoPesquisado;
	
	@SuppressWarnings("unchecked")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_list_hinos_filtro);
		getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.color.beige));
		ArrayList<Hino> listaHinos = (ArrayList<Hino>) getIntent().getExtras().get("listaHinos");
		textoPesquisado = getIntent().getExtras().getString("textoPesquisado");
		listViewHinos = (ListView) findViewById(R.id.list_hinos_filtros);
		listViewHinos.setAdapter(new SimpleAdapter(this, listarHinos(listaHinos), R.layout.layout_list_hinos_filtro, from, to));
		listViewHinos.setOnItemClickListener(this);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	public void onItemClick(AdapterView<?> adapterView, View view, int index, long arg3) {
		HashMap hino = (HashMap) adapterView.getAdapter().getItem(index);
		try {
			Field field;
			field = R.raw.class.getDeclaredField("hino" + hino.get("id"));
			field.setAccessible(true);
			inoSelecionado = (Integer) field.get(new R.raw());
			Intent intent = new Intent(this, HinoActivity.class);
			Bundle bundle = new Bundle();
			bundle.putInt("inoSelecionado", inoSelecionado);
			bundle.putString("idHino", hino.get("id").toString());
			intent.putExtras(bundle);
			startActivity(intent);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public List<Map<String, Object>> listarHinos(ArrayList<Hino> listaHinos) {
		hinos = new ArrayList<Map<String, Object>>();
		for (Hino item : listaHinos) {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("id", item.getId().toString());
			map.put("nome", item.getNome());
			map.put("text_complemento", retornaLetraPesquisada(item.getId()));
			hinos.add(map);
		}
		return hinos;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getTitle().toString().equals(getString(R.string.app_name))) {
			this.finish();
		}
		return super.onOptionsItemSelected(item);
	}
	
	public String retornaLetraPesquisada(String idHino) {
		String retorno = "";
		try {
			Field field = R.raw.class.getDeclaredField("hino" + idHino);
			field.setAccessible(true);
			Integer inoSelecionado = (Integer) field.get(new R.raw());
			InputStream is = getResources().openRawResource(inoSelecionado);
			BufferedReader br = null;
			try {
				br = new BufferedReader(new InputStreamReader(is, "ISO-8859-1"));
			} catch (UnsupportedEncodingException e1) {
				e1.printStackTrace();
			}
			String line;
			int count = 0;
			try {
				while ((line = br.readLine()) != null) {
					if (count > 2) {
						if(line.toLowerCase().contains(textoPesquisado.toLowerCase())) {
							retorno = line;
							break;
						}
					}
					count++;
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retorno;
	}
	
}
