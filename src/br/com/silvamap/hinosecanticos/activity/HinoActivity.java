package br.com.silvamap.hinosecanticos.activity;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import com.ace.hinoscanticos.R;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ShareCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.ShareActionProvider;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;
import br.com.silvamap.hinosecanticos.dao.FavoritoSQLiteHelper;
import br.com.silvamap.hinosecanticos.dao.HinoSQLiteHelper;
import br.com.silvamap.hinosecanticos.dao.LetraSQLiteHelper;
import br.com.silvamap.hinosecanticos.entidade.Favorito;

@SuppressLint("NewApi")
public class HinoActivity extends ActionBarActivity {

	private TextView textoHino;
	private TextView tituloHino;
	private TextView autorHino;
	private float zoom = 20;
	private ShareActionProvider shareActionProvider;
	private String idHino;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_hino);
		getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.color.beige));
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowTitleEnabled(false);
		textoHino = (TextView) findViewById(R.id.textoHino);
		tituloHino = (TextView) findViewById(R.id.tituloHino);
		autorHino = (TextView) findViewById(R.id.autorHino);
		textoHino.setTextSize(zoom);
		idHino = getIntent().getExtras().getString("idHino");
		carregaHino();
	}

	private void carregaHino() {
		LetraSQLiteHelper letraDAO = new LetraSQLiteHelper(this);
		HinoSQLiteHelper hinoDAO = new HinoSQLiteHelper(this);
		InputStream is = getResources().openRawResource(getIntent().getExtras().getInt("inoSelecionado"));
		BufferedReader br = null;
		try {
			br = new BufferedReader(new InputStreamReader(is, "ISO-8859-1"));
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		StringBuilder builder = new StringBuilder();
		String line;
		int count = 0;
		try {
			letra:while ((line = br.readLine()) != null) {
				if (count < 3) {
					switch (count) {
					case 0:
						tituloHino.setText(completarZeroEsquerda(idHino) + " - " + hinoDAO.recuperarPorId(Integer.parseInt(idHino)).getNome());
						break;
					case 1:
						autorHino.setText(line);
						break letra;
					default:
						break;
					}
				} else {
					builder.append(line + "\n");
				}
				count++;
			}
			textoHino.setText(letraDAO.recuperarPorHino(idHino).getLetra());
		} catch (IOException e) {
			e.printStackTrace();
		}
		letraDAO.close();
		hinoDAO.close();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.hino_menu, menu);
	    MenuItem item = menu.findItem(R.id.menu_item_share);
	    shareActionProvider = new ShareActionProvider(this);
	    MenuItemCompat.setActionProvider(item, shareActionProvider);
	    shareActionProvider.setShareIntent(compartilharLetra());
	    
	    MenuItem itemFavorite = menu.findItem(R.id.menu_item_favorite);
	    FavoritoSQLiteHelper db = new FavoritoSQLiteHelper(this);
	    if(db.hinoFavorito(idHino)) {
	    	itemFavorite.setIcon(R.drawable.ic_action_favorite_yellow);
	    } else {
	    	itemFavorite.setIcon(R.drawable.ic_action_favorite);
	    }
	    db.close();
		return super.onCreateOptionsMenu(menu);
	}
	
	public Intent compartilharLetra() {
	    String yourShareText = tituloHino.getText().toString() + "\n" + autorHino.getText().toString() + "\n\n" + textoHino.getText().toString() + "\nHinos & C�nticos";
	    Intent shareIntent = ShareCompat.IntentBuilder.from(this).setType("text/plain").setText(yourShareText).getIntent();
	    return shareIntent;
	}
	

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getTitle().toString().equals(getString(R.string.app_name))) {
			this.finish();
		}
		return super.onOptionsItemSelected(item);
	}

	public void maisZoom(MenuItem view) {
		if (zoom <= 30) {
			zoom += 1;
			textoHino.setTextSize(zoom);
		}
	}

	public void menosZoom(MenuItem view) {
		if (zoom >= 15) {
			zoom -= 1;
			textoHino.setTextSize(zoom);
		}
	}
	
	public void favorito(MenuItem item) {
		FavoritoSQLiteHelper db = new FavoritoSQLiteHelper(this);
		if (!db.hinoFavorito(idHino)) {
			Favorito favorito = new Favorito();
			favorito.setIdHino(idHino);
			db.insert(favorito);
			item.setIcon(R.drawable.ic_action_favorite_yellow);
			Toast.makeText(this, "Hino adicionado aos seus favoritos!", Toast.LENGTH_SHORT).show();
		} else {
			db.delete(idHino);
			item.setIcon(R.drawable.ic_action_favorite);
			Toast.makeText(this, "Hino removido aos seus favoritos!", Toast.LENGTH_SHORT).show();
		}
		db.close();
	}
	
	public String completarZeroEsquerda(String numeroHino) {
		return String.format("%03d", Integer.parseInt(numeroHino));
	}

}
