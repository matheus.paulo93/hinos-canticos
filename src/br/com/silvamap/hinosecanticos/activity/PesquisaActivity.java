package br.com.silvamap.hinosecanticos.activity;

import java.util.ArrayList;
import java.util.List;

import com.ace.hinoscanticos.R;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import br.com.silvamap.hinosecanticos.dao.HinoSQLiteHelper;
import br.com.silvamap.hinosecanticos.entidade.Hino;
import br.com.silvamap.hinosecanticos.fragment.FragmentTab1;
import br.com.silvamap.hinosecanticos.listener.MyTabListener;

public class PesquisaActivity extends ActionBarActivity {
	ActionBar.Tab tab1;
	Fragment fragmentTab1 = new FragmentTab1();
//	Fragment fragmentTab2 = new FragmentTab2();
//	Fragment fragmentTab3 = new FragmentTab3();
	public static TextView textview;
	public static EditText editText;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_pesquisa);
		
		ActionBar actionBar = getSupportActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		actionBar.setBackgroundDrawable(getResources().getDrawable(R.color.beige));
		actionBar.setDisplayHomeAsUpEnabled(true);

		tab1 = actionBar.newTab().setText("Pesquisa Geral");
//		tab2 = actionBar.newTab().setText("Estribilho");
//		tab3 = actionBar.newTab().setText("Autor");

		tab1.setTabListener(new MyTabListener(fragmentTab1));
//		tab2.setTabListener(new MyTabListener(fragmentTab2));
//		tab3.setTabListener(new MyTabListener(fragmentTab3));

		actionBar.addTab(tab1);
//		actionBar.addTab(tab2);
//		actionBar.addTab(tab3);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getTitle().toString().equals(getString(R.string.app_name))) {
			this.finish();
		}
		return super.onOptionsItemSelected(item);
	}
	
	public void pesquisar(View view) {
//		if (textview.getText().toString().equals(getString(R.string.pesquisa_primeiros_versos))) {
			HinoSQLiteHelper db = new HinoSQLiteHelper(this);
			List<Hino> listHinos = db.recuperarPorFiltro(editText.getText().toString());
			Bundle bundle = new Bundle();
			bundle.putSerializable("listaHinos", (ArrayList<Hino>)listHinos);
			bundle.putSerializable("textoPesquisado", editText.getText().toString());
			Intent intent = new Intent(this, ListHinosFiltro.class);
			intent.putExtras(bundle);
			startActivity(intent);
//		} else if (textview.getText().toString().equals(getString(R.string.pesquisa_autor))) {
//			HinoSQLiteHelper db = new HinoSQLiteHelper(this);
//			ArrayList<Hino> listHinos = db.recuperarPorAutor(editText.getText().toString());
//			Bundle bundle = new Bundle();
//			bundle.putSerializable("listaHinos", listHinos);
//			Intent intent = new Intent(this, ListHinosFiltro.class);
//			intent.putExtras(bundle);
//			startActivity(intent);
//		} else if (textview.getText().toString().equals(getString(R.string.pesquisa_estribilho))) {
//			HinoSQLiteHelper db = new HinoSQLiteHelper(this);
//			ArrayList<Hino> listHinos = db.recuperarPorEstribilho(editText.getText().toString());
//			Bundle bundle = new Bundle();
//			bundle.putSerializable("listaHinos", listHinos);
//			Intent intent = new Intent(this, ListHinosFiltro.class);
//			intent.putExtras(bundle);
//			startActivity(intent);
//		}
	}
}
