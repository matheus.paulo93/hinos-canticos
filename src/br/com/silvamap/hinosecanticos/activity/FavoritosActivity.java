package br.com.silvamap.hinosecanticos.activity;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import br.com.silvamap.hinosecanticos.dao.HinoSQLiteHelper;
import br.com.silvamap.hinosecanticos.entidade.Hino;

import com.ace.hinoscanticos.R;

public class FavoritosActivity extends ActionBarActivity implements OnItemClickListener {

	private List<Map<String, Object>> hinos;
	private int inoSelecionado;
	private String[] from = { "id", "nome", "text_complemento" };
	private int[] to = { R.id.numeroHinoFavorito, R.id.nomeHinoFavorito};
	private ListView listViewHinos;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_list_hinos_filtro);
		getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.color.beige));
		HinoSQLiteHelper db = new HinoSQLiteHelper(this);
		ArrayList<Hino> listaHinos = db.recuperarTodosFavoritos();
		db.close();
		listViewHinos = (ListView) findViewById(R.id.list_hinos_filtros);
		listViewHinos.setAdapter(new SimpleAdapter(this, listarHinos(listaHinos), R.layout.layout_list_hinos_favoritos, from, to));
		listViewHinos.setOnItemClickListener(this);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	public void onItemClick(AdapterView<?> adapterView, View view, int index, long arg3) {
		HashMap hino = (HashMap) adapterView.getAdapter().getItem(index);
		try {
			Field field;
			field = R.raw.class.getDeclaredField("hino" + hino.get("id"));
			field.setAccessible(true);
			inoSelecionado = (Integer) field.get(new R.raw());
			Intent intent = new Intent(this, HinoActivity.class);
			Bundle bundle = new Bundle();
			bundle.putInt("inoSelecionado", inoSelecionado);
			bundle.putString("idHino", hino.get("id").toString());
			intent.putExtras(bundle);
			startActivity(intent);
			this.finish();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public List<Map<String, Object>> listarHinos(ArrayList<Hino> listaHinos) {
		hinos = new ArrayList<Map<String, Object>>();
		for (Hino item : listaHinos) {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("id", item.getId().toString());
			map.put("nome", item.getNome());
			hinos.add(map);
		}
		return hinos;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getTitle().toString().equals(getString(R.string.favoritos))) {
			this.finish();
		}
		return super.onOptionsItemSelected(item);
	}
	
}
