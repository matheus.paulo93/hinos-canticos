package br.com.silvamap.hinosecanticos.activity;

import android.content.res.Resources.NotFoundException;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import com.ace.hinoscanticos.R;

public class SobreActivity extends ActionBarActivity {
	
	private TextView textViewPrefacio16;
	private TextView textViewPrefacio17;
	private TextView textViewPrefacio18;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sobre);
		getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.color.beige));
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		
		textViewPrefacio16 = (TextView) findViewById(R.id.prefacio16);
		textViewPrefacio17 = (TextView) findViewById(R.id.prefacio17);
		textViewPrefacio18 = (TextView) findViewById(R.id.prefacio18);
		
		try {
			InputStreamReader isr = new InputStreamReader(getResources().openRawResource(R.raw.prefacio16), "ISO-8859-1");
			BufferedReader reader = new BufferedReader(isr);
			StringBuilder stringBuilder = new StringBuilder();
			String line;
			while((line = reader.readLine()) != null) {
				stringBuilder.append(line);
			}
			textViewPrefacio16.setText(stringBuilder.toString());
			
			isr = new InputStreamReader(getResources().openRawResource(R.raw.prefacio17), "ISO-8859-1");
			reader = new BufferedReader(isr);
			stringBuilder = new StringBuilder();
			while((line = reader.readLine()) != null) {
				stringBuilder.append(line);
			}
			textViewPrefacio17.setText(stringBuilder.toString());
			
			isr = new InputStreamReader(getResources().openRawResource(R.raw.prefacio18), "ISO-8859-1");
			reader = new BufferedReader(isr);
			stringBuilder = new StringBuilder();
			while((line = reader.readLine()) != null) {
				stringBuilder.append(line);
			}
			textViewPrefacio18.setText(stringBuilder.toString());
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (NotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if(item.getTitle().toString().equals(getString(R.string.app_name))) {
			this.finish();
		}
		return super.onOptionsItemSelected(item);
	}
	
}
