package br.com.silvamap.hinosecanticos.activity;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import com.ace.hinoscanticos.R;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import br.com.silvamap.hinosecanticos.dao.AutorSQLiteHelper;
import br.com.silvamap.hinosecanticos.dao.HinoSQLiteHelper;
import br.com.silvamap.hinosecanticos.dao.LetraSQLiteHelper;
import br.com.silvamap.hinosecanticos.entidade.Autor;
import br.com.silvamap.hinosecanticos.entidade.Hino;
import br.com.silvamap.hinosecanticos.entidade.Letra;

@SuppressLint("UseSparseArrays")
public class MainActivity extends Activity {

	private ProgressDialog progressDialog;

	private List<Hino> listaHino = new ArrayList<Hino>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		HinoSQLiteHelper db = new HinoSQLiteHelper(this);
		Integer count = db.getCount();
		if (count > 0) {
			new Handler().postDelayed(new Runnable() {
				@Override
				public void run() {
					Intent i = new Intent(MainActivity.this, ListHinosActivity.class);
					startActivity(i);
					finish();
				}
			}, 1000);
		} else {
			carregaHinos();
			new BarraProgressoViewTask().execute();
		}
		db.close();
	}

	private void carregaHinos() {
		try {
			InputStream is = getResources().openRawResource(R.raw.hinos);
			BufferedReader br = new BufferedReader(new InputStreamReader(is, "ISO-8859-1"));

			String line;
			while ((line = br.readLine()) != null) {
				Hino hino = new Hino();
				hino.setId(line.split(";")[0]);
				hino.setNome(line.split(";")[1]);
				listaHino.add(hino);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void insereLetra(Integer idHino) {
		LetraSQLiteHelper db = new LetraSQLiteHelper(this);
		Field field;
		try {
			field = R.raw.class.getDeclaredField("hino" + idHino);
			field.setAccessible(true);
			Integer inoSelecionado = (Integer) field.get(new R.raw());
			String letraStr = carregaLetraHino(inoSelecionado);
			Letra letra = new Letra();
			letra.setIdHino(idHino);
			letra.setLetra(letraStr);
			db.insert(letra);
		} catch (NoSuchFieldException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}
		db.close();
	}

	private void insereAutor(Integer idHino) {
		AutorSQLiteHelper db = new AutorSQLiteHelper(this);
		Field field;
		try {
			field = R.raw.class.getDeclaredField("hino" + idHino);
			field.setAccessible(true);
			Integer inoSelecionado = (Integer) field.get(new R.raw());
			String nomeAutor = carregaAutorHino(inoSelecionado);
			Autor autor = new Autor();
			autor.setIdHino(idHino);
			autor.setNomeAutor(nomeAutor);
			db.insert(autor);
		} catch (NoSuchFieldException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}
		db.close();
	}

	private String carregaLetraHino(Integer inoSelecionado) {
		InputStream is = getResources().openRawResource(inoSelecionado);
		BufferedReader br = null;
		try {
			br = new BufferedReader(new InputStreamReader(is, "ISO-8859-1"));
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}
		StringBuilder builder = new StringBuilder();
		String line;
		int count = 0;
		try {
			while ((line = br.readLine()) != null) {
				if (count > 2) {
					builder.append(line + "\n");
				}
				count++;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return builder.toString();
	}

	private String carregaAutorHino(Integer inoSelecionado) {
		InputStream is = getResources().openRawResource(inoSelecionado);
		BufferedReader br = null;
		try {
			br = new BufferedReader(new InputStreamReader(is, "ISO-8859-1"));
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}
		StringBuilder builder = new StringBuilder();
		String line;
		int count = 0;
		try {
			while ((line = br.readLine()) != null) {
				if (count == 1) {
					builder.append(line + " ");
					break;
				}
				count++;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return builder.toString();
	}

	// private void insereEstribilho(Estribilho estribilho) {
	// EstribilhoSQLiteHelper db = new EstribilhoSQLiteHelper(this);
	// db.insert(estribilho);
	// }

	@Override
	public void onBackPressed() {
		super.onBackPressed();
	}

	// To use the AsyncTask, it must be subclassed
	private class BarraProgressoViewTask extends AsyncTask<Void, Integer, Void> {
		// Before running code in separate thread
		@Override
		protected void onPreExecute() {
			progressDialog = new ProgressDialog(MainActivity.this);
			progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
			progressDialog.setTitle("Atualizando hinos...");
			progressDialog.setMessage("Atualizando hinos, por favor espere.");
			progressDialog.setCancelable(false);
			progressDialog.setIndeterminate(false);
			progressDialog.setMax(listaHino.size());
			progressDialog.setProgress(0);
			progressDialog.show();
		}

		// The code to be executed in a background thread.
		@Override
		protected Void doInBackground(Void... params) {
			HinoSQLiteHelper db = new HinoSQLiteHelper(MainActivity.this);
			int counter = 0;
			while (counter <= listaHino.size() && listaHino.size() > 0) {
				try {
					db.insert(listaHino.get(counter));
					insereLetra(Integer.parseInt(listaHino.get(counter).getId()));
					insereAutor(Integer.parseInt(listaHino.get(counter).getId()));
				} catch (Exception e) {
					e.printStackTrace();
				}
				counter++;
				publishProgress(counter);
			}
			db.close();
			return null;
		}

		@Override
		protected void onProgressUpdate(Integer... values) {
			// set the current progress of the progress dialog
			progressDialog.setProgress(values[0]);
		}

		// after executing the code in the thread
		@Override
		protected void onPostExecute(Void result) {
			// close the progress dialog
			progressDialog.dismiss();
			// initialize the View
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			startActivity(new Intent(MainActivity.this, ListHinosActivity.class));
		}
	}
}
