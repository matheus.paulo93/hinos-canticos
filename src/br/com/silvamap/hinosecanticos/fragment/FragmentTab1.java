package br.com.silvamap.hinosecanticos.fragment;

import com.ace.hinoscanticos.R;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import br.com.silvamap.hinosecanticos.activity.PesquisaActivity;

public class FragmentTab1 extends Fragment {

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.tab, container, false);
		TextView textview = (TextView) view.findViewById(R.id.textview_pesquisa);
		textview.setText(getString(R.string.pesquisa_geral));
		PesquisaActivity.editText = (EditText) view.findViewById(R.id.edit_text_pesquisa);
		PesquisaActivity.editText.setText("");
		PesquisaActivity.textview = (TextView) view.findViewById(R.id.textview_pesquisa);
		return view;
	}

}