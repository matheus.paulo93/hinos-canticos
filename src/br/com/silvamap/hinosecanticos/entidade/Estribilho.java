package br.com.silvamap.hinosecanticos.entidade;

import java.io.Serializable;

public class Estribilho implements Serializable {

	private static final long serialVersionUID = 1L;

	// Nome da Tabela Primeiros Versos.
	public static final String TABLE_NAME = "estribilho";

	// Colunas da Tabela Primeiros Versos.
	public static final String ID = "id";
	public static final String ID_HINO = "id_hino";
	public static final String ESTRIBILHO = "estribilho";
	public static final String[] COLUMNS = { ID, ID_HINO, ESTRIBILHO };

	private Integer id;
	private Integer idHino;
	private String estribilho;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getIdHino() {
		return idHino;
	}

	public void setIdHino(Integer idHino) {
		this.idHino = idHino;
	}

	public String getEstribilho() {
		return estribilho;
	}

	public void setEstribilho(String estribilho) {
		this.estribilho = estribilho;
	}
}
