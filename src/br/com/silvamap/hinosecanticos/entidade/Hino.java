package br.com.silvamap.hinosecanticos.entidade;

import android.annotation.SuppressLint;
import java.io.Serializable;

@SuppressLint("DefaultLocale")
public class Hino implements Serializable {

	private static final long serialVersionUID = 1L;

	//Nome da Tabela Hino.
	public static final String TABLE_NAME = "hino";
	
	//Colunas da Tabela Hino.
	public static final String ID = "id";
	public static final String NOME = "nome";
	public static final String[] COLUMNS = {ID, NOME};
	
	private String id;
	private String nome;
	private PrimeirosVersos primeirosVersos;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Override
	public String toString() {
		return getId().toString().concat(": ").concat(getNome());
//		return getNome();
	}

	public PrimeirosVersos getPrimeirosVersos() {
		return primeirosVersos;
	}

	public void setPrimeirosVersos(PrimeirosVersos primeirosVersos) {
		this.primeirosVersos = primeirosVersos;
	}

}
