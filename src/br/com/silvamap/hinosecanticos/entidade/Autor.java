package br.com.silvamap.hinosecanticos.entidade;

import java.io.Serializable;

public class Autor implements Serializable {

	private static final long serialVersionUID = 1L;
	
	//Nome da table Autor
	public static final String TABLE_NAME = "autor";
	
	//Colunas
	public static String ID = "id";
	public static String ID_HINO = "id_hino";
	public static String NOME_AUTOR = "nome_autor";
	
	private Integer id;
	private Integer idHino;
	private String nomeAutor;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getIdHino() {
		return idHino;
	}

	public void setIdHino(Integer idHino) {
		this.idHino = idHino;
	}

	public String getNomeAutor() {
		return nomeAutor;
	}

	public void setNomeAutor(String nomeAutor) {
		this.nomeAutor = nomeAutor;
	}

}
