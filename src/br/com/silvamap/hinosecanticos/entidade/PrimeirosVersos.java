package br.com.silvamap.hinosecanticos.entidade;

import java.io.Serializable;

public class PrimeirosVersos implements Serializable {

	private static final long serialVersionUID = 1L;

	// Nome da Tabela Primeiros Versos.
	public static final String TABLE_NAME = "primeiros_versos";

	// Colunas da Tabela Primeiros Versos.
	public static final String ID = "id";
	public static final String ID_HINO = "id_hino";
	public static final String VERSOS = "versos";
	public static final String[] COLUMNS = { ID, ID_HINO, VERSOS };

	private Integer id;
	private Integer idHino;
	private String versos;

	public Integer getId() {
		return id;
	}

	public Integer getIdHino() {
		return idHino;
	}

	public void setIdHino(Integer idHino) {
		this.idHino = idHino;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getVersos() {
		return versos;
	}

	public void setVersos(String versos) {
		this.versos = versos;
	}

}
