package br.com.silvamap.hinosecanticos.entidade;

import java.io.Serializable;

public class Letra implements Serializable {

	private static final long serialVersionUID = 1L;

	public static final String TABLE_NAME = "letra";

	public static final String ID = "id";
	public static final String ID_HINO = "id_hino";
	public static final String LETRA = "letra";
	public static final String[] COLUMNS = { ID, ID_HINO, LETRA };

	private Integer id;
	private Integer idHino;
	private String letra;

	public String getLetra() {
		return letra;
	}

	public void setLetra(String letra) {
		this.letra = letra;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getIdHino() {
		return idHino;
	}

	public void setIdHino(Integer idHino) {
		this.idHino = idHino;
	}
}
