package br.com.silvamap.hinosecanticos.entidade;

import java.io.Serializable;

public class Favorito implements Serializable{

	private static final long serialVersionUID = 1L;

	//Nome da Tabela Hino.
	public static final String TABLE_NAME = "favorito";
	
	//Colunas da Tabela Hino.
	public static final String ID = "id";
	public static final String ID_HINO = "id_hino";
	public static final String[] COLUMNS = {ID, ID_HINO};
	
	private String id;
	private String idHino;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getIdHino() {
		return idHino;
	}
	public void setIdHino(String idHino) {
		this.idHino = idHino;
	}
	
}
