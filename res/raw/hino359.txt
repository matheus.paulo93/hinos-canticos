Hino 359  Deus Buscou-me 
Henry Mawxell Wright

1. Deus buscou-me e ensinou-me
Que do mundo resgatou-me (bis)
Para Si, para Si!
Resgatado, perdoado,
Ele a mim quer ao Seu lado, (bis)
Junto a Si, junto a Si!

Deus 'st� perto. Oh, qu�o perto!
Carinhoso quer, decerto,
Carinhoso quer, decerto,
Me valer, me valer!

2. Qu�o bondosa, gloriosa,
Essa gra�a majestosa (bis)
De Jesus, de Jesus!
Redimiu-me e seguiu-me;
Nos Seus bra�os conduziu-me (bis)
Para a luz, para a luz!

3. Vem guiar-me, segurar-me,
Sobre a Rocha vem firmar-me (bis)
Com amor, com amor!
Protegidos, socorridos,
S�o Seus filhos escolhidos (bis)
Do Senhor, do Senhor!

4. Lan�o ao vento meu tormento,
Pois que tenho o juramento (bis)
Que me deu, que me deu!
Segredando, animando,
Ou�o a voz de Deus, clamando: (bis)
"Sim, �s Meu! Sim, �s Meu!"
