Hino 154  Podem Todos Vir 
Stuart Edmund Mc Nair

1. "Deixai que venham a Mim meninos",
Outrora disse Jesus assim;
E Sua voz ainda vos chama:
"Vinde, meninos. Oh! Vinde a Mim!" 

Podem todos vir aos p�s do Salvador,
Entoar-Lhe gratos hinos de louvor.
Cristo n�o muda, nem mudar�;
Inda as crian�as receber�.

2. Pouco depois de assim convidar-nos,
Cristo na cruz por n�s padeceu;
Muitas crian�as j� t�m com Ele
Parte nos santos gozos do c�u.

3. Quantos meninos, inda descrentes,
N�o v�m a voz de Cristo escutar;
Como fazia o pr�digo filho,
Buscam seu gozo longe do lar.

4. Cristo, que deu por n�s Sua vida,
Vive por n�s agora nos c�us;
Mui brevemente vem recolher-nos
Para vivermos sempre com Deus. 