Hino 056  Substitui��o
Dillwyn Mac Fadden Hazlett

1. Morri na cruz por ti, morri pra te livrar;
Meu sangue, sim, verti e posso te salvar.

Morri, morri na cruz por ti:
Que fazes tu por Mim?

2. Vivi no mundo a� com dor, com dissabor;
Sim, tudo fiz por ti, pra ser teu Salvador.

3. Sofri na cruz por ti a fim de te salvar;
A vida consegui que podes j� gozar.

4. Oferto a salva��o, dos altos c�us favor;
� livre o Meu perd�o, � grande o Meu amor. 