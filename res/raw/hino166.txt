Hino 166  Eis que Pelos Vales 
Henry Maxwell Wright

1. Eis que pelos vales, montes e ribeiros,
Cristo, o Bom Pastor, busca Seus cordeiros!
Com bondade imensa, com paciente amor,
Quer seguros t�-los seu Salvador!

2. Do meu Bom Pastor sou Seu cordeirinho,
E seguro estou, inda que fraquinho!
Quem me arrancar� da Sua santa m�o,
Se entregar a Cristo meu cora��o? 