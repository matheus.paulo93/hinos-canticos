Hino 219  Que Ser�? 
Stuart Edmund Mc Nair

1. N�s falamos do mundo ditoso
E dos gozos que Deus nos dar�;
Do pa�s refulgente e formoso,
Mas, estarmos ali, que ser�?

2. N�s falamos, saudosos, da gl�ria
Que o remido, no c�u, gozar�;
De seus hinos de amor e vit�ria,
Mas, estarmos ali, que ser�?

3. N�s falamos do dia esplendente
Que no santo pa�s brilhar�;
De Jesus, o Cordeiro paciente,
Mas, estarmos ali, que ser�?

4. Nem pecados, nem choro ou tristeza,
Nem pesares alguns haver�
Na presen�a de Deus, com certeza,
Mas, estarmos ali, que ser�? 