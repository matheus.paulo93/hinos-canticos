Hino 388  A Chegada do Senhor 
Jos� Il�dio Freire

1. H� tremor em toda a terra,
H� geral inquieta��o
E rumor de nova guerra
De na��o contra na��o.
S�o sinais de estar bem perto
A chegada do Senhor;
Pode o crente estar mui certo
De ver breve o Salvador.

2. Vamos ver o Bem-Amado,
Sua gl�ria partilhar;
Quem aqui foi rejeitado
H� de vir enfim reinar.
Oh, que dia t�o glorioso
Em que os povos h�o de ver
Um Rei justo e poderoso,
A Quem h�o de obedecer!

3. E, se o dia n�o sabemos
Em que Cristo h� de voltar,
Vigiar, ent�o, devemos,
E viver pra Lhe agradar.
Bem merece que O sirvamos,
Trabalhando com amor.
Sim! Irm�os, irm�os, digamos:
"Vem, oh, vem, Jesus Senhor!"