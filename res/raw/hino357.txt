Hino 357  Falai de Cristo 
Henry Maxwell Wright

1. Sabeis falar de tudo que neste mundo est�,
Mas nem sequer palavra do Deus que tudo d�!

Irm�os, irm�os, falemos do nosso Salvador!
Oremos, ou cantemos e demos-Lhe louvor!

2. Falamos do mau tempo, do frio ou do calor;
Qu�o bem melhor seria falar do Salvador!

3. Falamos uns dos outros, e quantas vezes mal!
Irm�os, falai de Cristo, o Amigo sem igual!

4. Falemos da bondade do grande Salvador,
Da Sua terna gra�a, do Seu imenso amor!

5. Da cruz, tamb�m, falemos: ali Jesus quis dar
Seu sangue precioso e assim nos resgatar! 

6. Falemos da maneira como Ele nos salvou:
Amando, amando sempre, at� que nos ganhou! 
