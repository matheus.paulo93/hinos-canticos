Hino 503  A expia��o se fez
Richard Holden

1. A expia��o se fez, a v�tima morreu,
E Cristo de uma vez passou pra al�m do v�u,
E agora faz intercess�o,
Pont�fice maior que Ar�o.

2. Erguido a m�os n�o foi o templo onde Ele est�;
Jamais de bode ou boi se esparge o sangue l�.
Por sangue pr�prio ali entrou,
Que por n�s outros derramou.

3. Fazendo ali valer o que Ele fez na cruz,
Consegue assim manter em comunh�o, na luz
E puros do pecado, os Seus,
No santu�rio do Seu Deus. 

4. Oculto agora est� da natural vis�o,
Por�m Se mostrar� aos que de cora��o
O esperam; pois que vem aqui
Buscar Seus santos para Si.