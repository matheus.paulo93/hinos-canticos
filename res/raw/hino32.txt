Hino 032  S� Um Passo 
Salom�o Luiz Ginsburg

1. Mui terna e mui doce do Mestre � a voz,
Chamando-me com amor:
"De bra�os abertos te espero, vem j�;
Oh! Vem ao teu Redentor!"

Cristo me chama, me quer salvar.
� s� um passo que tenho de dar;
Quero achegar-me, Senhor, a Ti,
Para nunca Te abandonar!

2. "Tens muitos pecados e vives sem f�",
Sugere-me o tentador.
"Eu tudo j� fiz", me segreda outra voz,
"Confia em teu Salvador".

3. "Mui fr�gil me sinto, receio cair!",
Com medo ainda aleguei.
� alma, n�o temas, pois Cristo te diz:
"Eu n�o te abandonarei".

4. O mundo perdido nas trevas est�,
Seu gozo � s� ilus�o. 
Amor, vida e paz me concede Jesus,
Sou dEle de cora��o. 