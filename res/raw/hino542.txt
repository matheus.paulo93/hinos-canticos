Hino 542  Senhor, Louvamos 
Richard Holden

1. N�s a Ti, Senhor, louvamos
Com alegre cora��o,
Pela dita que gozamos,
Desfrutando a salva��o.
Teu Jesus Tu enviaste
Desde o c�u pra nos salvar,
Teu Esp�rito incumbiste
Da miss�o de nos chamar. 

2. Mesmo estando o pre�o pago
Para a nossa reden��o,
Nada t�nhamos lucrado
E ficava tudo em v�o;
Mas a gra�a, que nos dera
Cristo para nos remir,
Teu Esp�rito enviou-nos
Para as almas influir. 

3. Teu bendito Mensageiro
Mui distante nos achou;
Cora��es empedernidos
Em n�s outros encontrou.
Com esfor�os incans�veis,
Com ternura, com amor,
N�o cessou de persuadir-nos
A chegarmos ao Senhor. 

4. Entre as trevas que nos cercam
Fez raiar a Tua luz;
Fez-nos ver a Tua gl�ria
Em o rosto de Jesus.
Devedores sempre somos,
Nosso Deus e Pai, aqui,
Pela gra�a que nos salva
E nos leva para Ti.