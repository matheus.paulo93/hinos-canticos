Hino 440  O Senhor � o meu Pastor 
Luiz Soares

1. O meu Pastor � meu Senhor,
O Eterno Jeov�,
E nada pode me faltar,
Pois Ele tudo d�. 

2. Nos verdejantes pastos Seus
Em paz me faz deitar;
�s �guas quietas me conduz
E faz-me repousar. 

3. Se fraco estou, mui pronto est�
Minha alma a restaurar,
E, na justi�a e retid�o,
Meus fracos p�s guiar. 

4. No vale escuro eu posso andar,
Pois Tu comigo est�s;
E mal nenhum eu temerei,
Pois Tu me guardar�s! 

5. Perante as hostes vis do mal
Que ao derredor est�o,
Puseste a mesa para mim
Com farta provis�o. 

6. Com �leo santo vens me ungir
E assim me confortar,
E de alegria vens encher
Meu copo a transbordar.

7. Por toda a minha vida aqui
Terei o Seu amor
E, para sempre, habitarei
Na casa do Senhor.