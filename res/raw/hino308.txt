Hino 308  Face a Face 
Stuart Edmund Mc Nair

1. Um dia a lida acabar�
E meu descanso gozarei;
Na gl�ria que Jesus me d�,
O Seu amor desfrutarei.

E face a face v�-lO-ei:
"Por gra�a salvo", cantarei (bis)

2. Um dia o corpo natural
Qual o do meu Senhor ser�: 
Transforma��o celestial,
E dor n�o mais lugar ter�.

3. Qu�o rica tal consuma��o:
Com Ele estar, para O servir!
Que triunfante adora��o
Espero dar-Lhe no porvir! 		     