Hino 082  Sempre Alegre Em Jesus 
An�nimo

1. Com o rosto alegre vou anunciar
O Evangelho santo em qualquer lugar.

Gozo, tenho gozo; Tenho gozo no Senhor Jesus!
Triste? Nunca triste! Sempre alegre em Sua luz.

2. Pecador convicto, fui ter com Jesus;
NEle achei conforto, mesmo ao p� da cruz.

3. Ele, em minhas lutas, me proteger�;
Ficarei contente: Deus comigo est�! 