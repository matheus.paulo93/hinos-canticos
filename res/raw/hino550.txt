Hino 550  No Santu�rio 
Richard Holden

1. Por Cristo tendo achado entrada para os c�us,
No Santu�rio entramos em plena paz com Deus;
Celeste Santu�rio, o divinal lugar,
No qual ao Pai podemos no Esp�rito adorar.

2. Com regozijo vimos, com hinos de louvor,
Alegres entoamos as gl�rias do Senhor,
Fragr�ncias difundindo de grata adora��o,
�quele que � t�o digno de toda a exalta��o. 

3. Pont�fice divino havendo em Teu Jesus,
Por Ele nos chegamos para a celeste luz;
E nossos sacrif�cios de cordial louvor,
Por Sua m�o passando, v�o para Ti, Senhor! 

4. Eterna gl�ria demos ao Deus supremo e bom,
Em coro de louvores, por Seu celeste Dom;
Ao Santo, Santo, Santo, excelso no saber,
Supremo em majestade, o sempiterno Ser!