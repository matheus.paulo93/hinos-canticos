Hino 114  Sempre o mesmo Salvador 
Henry Maxwell Wright e Luiz Soares

1. Se a salva��o eterna deveras desejais,
Oh, vinde sem demora, sim, vinde como estais!
Eis Cristo pronto a perdoar a todo pecador,
Pois Quem morreu na cruz por n�s
� o mesmo Salvador!

Vinde j�, vinde j�, (bis)
Vinde ao mesmo Salvador! 

2. Jesus, com simpatia, enquanto aqui andou,
Bondade, gra�a e b�n��os a todos dispensou.
E ainda hoje, todos quantos venham ao Senhor,
Aqueles dons receber�o
Do mesmo Salvador!

3. Na cruz de Jesus Cristo foi feita a reden��o;
Chegai-vos, pois, a Ele e encontrareis perd�o.
Sem mais reservas confiai, rendei-vos ao Senhor,
E a vida eterna recebei
Do mesmo Salvador. 