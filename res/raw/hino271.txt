Hino 271  Cantando Aleluia 
Henry Maxwell Wright

1. Eis-nos aqui, Senhor Jesus,
Somente em Ti confiando,
Permanecendo em Teu amor,
Teu reino aqui buscando;
As nossas vidas, nosso amor,
A Ti trazemos, Salvador,
Cantando-Te: "Aleluia!"

2. Ao Evangelho, � Salvador,
T�o puro e glorioso,
�s leis sant�ssimas que
Tu nos d�s, Senhor bondoso,
"Am�m!", dizemos com fervor
E, pelo Teu imenso amor,
Clamamos: "Aleluia!" 