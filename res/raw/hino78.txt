Hino 078  � Pr�digo Vem 
Sarah Poulton Kalley

1. Vem, filho perdido! � pr�digo, vem!
Ru�na te espera nas trevas al�m!
Tu, de medo tremendo! Tu, de fome gemendo!

� filho perdido! Vem, pr�digo, vem!
Vem! Vem! � pr�digo, vem!

2. Vem, filho perdido! � pr�digo, vem!
Teu Pai te convida, querendo-Te bem!
Vestes h� para ornar-te, ricos dons; vem fartar-te!

3. Vem, filho perdido! Oh, volta a Jesus!
Bondade infinita se avista na cruz.
Em mis�rias vagando, tuas culpas chorando,

4. � pr�digo, escuta a chamada de amor!
Oh, rompe as ciladas do vil tentador!
Pois em casa h� bastante e tu andas errante!