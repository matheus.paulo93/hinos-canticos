Hino 248  Oh, Vem Senhor 
Richard Holden

1. Oh, vem, Senhor Jesus, depressa vem!
Teu povo espera com amor por Ti.
Sentindo a falta, neste mundo aqui,
Do seu Senhor!

2. Oh, vem Senhor Jesus, pois s� em Ti
Achamos gozo, bem e perfei��o,
Que n�o se encontram neste mundo v�o.
Oh, vem Senhor! 

3. Oh, vem, Senhor Jesus! Sim, vem pra n�s.
Pois nosso cora��o anela ver
Teu rosto amado e a presen�a ter
Do seu Senhor!

4. Oh, vem, Senhor Jesus! Sabemos bem
Que Tu nos amas e Tua alma quer
A Tua Igreja ali, na gl�ria ter;
Vem, pois, Senhor! 	