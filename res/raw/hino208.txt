Hino 208  Onde tudo � feliz
William Edwin Entzminger

1. � de prova��o a nossa vida aqui?
Vamo-la deixar um dia;
Alegria e gozo vamos ter ali,
Onde tudo � mui feliz.

Vamos ver o Rei ali, Sua santa paz fruir,
Com Jesus morar, Seu rosto contemplar,
Grande gozo desfrutar.

2. A vit�ria � certa que Jesus nos d�,
Vamo-la gozar um dia;
A peleja finda, calma nos vir�
Onde tudo � mui feliz.

3. Muitos s�o os males que nos cercam c�,
Vamo-los deixar um dia;
Alegria plena n�s teremos l�,
Onde tudo � mui feliz.

4. Todos os remidos encontrar-se-�o
L� no lindo c�u, um dia; 
Alegria santa sempre ali ter�o,
Onde tudo � mui feliz. 	