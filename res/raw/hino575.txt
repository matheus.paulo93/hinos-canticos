Hino 575  O Rei da Gl�ria 
Henry Maxwell Wright

1. O Rei da gl�ria quem ser�,
Senhor onipotente?
S� Deus o Filho pode ser,
Que reina eternamente.
As chagas que Ele recebeu,
A morte que na cruz sofreu,
O provam cabalmente! 

2. O golpe que Ele � morte deu,
Ganhando assim vit�ria,
A sepultura que rompeu,
Subindo � excelsa Gl�ria,
Declaram Seu real poder
E ao mundo d�o a conhecer
Que � Cristo o Rei da gl�ria! 

3. Eis elevado l� no c�u
Jesus, o Rei da gl�ria!
Ali reinando, al�m do v�u,
Com Deus na eterna gl�ria,
Onde a remida multid�o,
Em triunfante adora��o,
A Cristo rende gl�ria!