Hino 120  Vinde V�s 
An�nimo

1. Vinde v�s que, na tristeza, caminhais sem luz;
Quem vos chama com ternura � Jesus!

2. Vida, gozo, paz, descanso, tudo vos ganhou
Pela cruz, onde o Seu sangue derramou.

3. N�o somente quer salvar-vos: quer-vos para Si,
Para serdes Seus amigos, mesmo aqui.

4. Tudo o mais abandonando, sem mais resistir,
Vinde, Seu amor e gra�a possuir! 