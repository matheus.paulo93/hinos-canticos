Hino 409  A voz do Pastor
Henry Maxwell Wright

1. Ou�am a voz do Bom Pastor
Que no deserto, com amor,
Busca as ovelhas que, no mal,
Andam bem longe do curral.

Com amor, com ardor,
Nos convida agora o Bom Pastor;
Com amor, com ardor,
Trabalhemos para Cristo.

2. Quem ao Pastor quer ajudar
Essas ovelhas a buscar?
Quem quer gui�-las ao redil,
Salvas do inimigo hostil?

3. Outras ovelhas inda est�o
Longe de Deus, sem salva��o;
Nunca o Pastor deseja ver
Qualquer ovelha se perder.