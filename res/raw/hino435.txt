Hino 435 - Vou Pelos Montes
Benjamin Rufino Duarte

1. Peregrinando vou pelos montes
E pelos vales, sempre na luz!
Cristo promete nunca deixar-me,
"Eis-Me convosco", disse Jesus. 

Brilho celeste! Brilho celeste!
Enche a minha alma a gl�ria de Deus!
Com aleluias, sigo cantando,
Canto louvores, indo pra os c�us! 

2. Sombras � roda, nuvens em cima
O Salvador n�o h�o de ocultar,
Ele � a luz que nunca se apaga,
Junto a Seu lado sempre hei de andar. 

3. V�o-me guiando raios benditos,
Que me conduzem para a mans�o;
Mais e mais perto, o Mestre seguindo,
Canto os louvores da salva��o.