Hino 113  Precisando Teu Perd�o 
Stuart Edmund Mc Nair

1. Precisando do perd�o,
Do meu mal a salva��o,
Luz, vigor, consola��o,
Venho a Ti, Senhor. 

2. Outro Redentor n�o h�;
Outro n�o me perdoar�,
Nem me purificar�;
Venho a Ti, Senhor.

3. Para s� de Ti obter
Gra�a, dire��o, poder;
Para a Tua gl�ria ver,
Venho a Ti, Senhor.

4. Por ser fraco, sem vigor,
Por saber que �s Redentor,
Pra chamar-Te meu Senhor,
Venho a Ti, Jesus.

5. Venho tal e qual estou;
Venho a Quem me resgatou.
Tudo em Ti minha alma achou!
Venho a Ti, Senhor. 