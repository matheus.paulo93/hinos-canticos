Hino 062  Atribulado cora��o
Ricardo Pitrowsky

1. Atribulado cora��o,
Em Cristo al�vio encontrar�s;
Consolo, paz e Seu perd�o,
Sim, dEle tu receber�s.

Oh! Vem sem demora ao Salvador!
Por que vacilar e ter temor? 
Oh! Vem! Vem j�!
Descanso te dar�!

2. Dilacerado pela dor
Das tuas culpas, do pecar,
Vem, sem demora, ao Salvador
E vida nova h�s de gozar.

3. Se, para vir ao Salvador,
Tu tens franquezas a vencer,
Oh! Vem, pois Ele em Seu amor
E em gra�a te dar� poder!

4. A Cristo sem demora vem,
Pois Ele almeja te valer;
E sempre quer buscar teu bem;
Confia nEle em teu viver! 