Hino 557  Eis que bondoso amor 
Henry Maxwell Wright

1. Eis que bondoso e imenso amor
Deus Pai nos revelou!
A n�s, a quem por filhos Seus
Em Cristo nos tomou! 

Que amor! Que amor sem igual!
Que amor sem igual
O nosso Deus nos revelou!
A n�s, at� mesmo a n�s
Que por filhos Seus tomou! 

2. Da morte � vida nos chamou,
Das trevas para a luz;
Pois, oh, qu�o junto a Si nos traz,
Completos em Jesus! 

3. Amados filhos somos j�,
Por Seu favor real,
E coerdeiros com Jesus
Da gl�ria divinal. 

4. O que seremos l� no c�u
Deus n�o nos descobriu;
Mas, para sermos como Ele �,
Jesus nos redimiu. 

5. Quem nEle, assim, espera aqui,
Qu�o puro deve ser!
De toda contamina��o
As vestes livres ter!