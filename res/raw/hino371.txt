Hino 371  H� trabalho pronto
Henry Maxwell Wright

1. H� trabalho pronto para ti, crist�o,
Que demanda toda a tua devo��o.
Vem alegremente a Cristo obedecer,
Pois s� tu, � crente, o poder�s fazer!

Por Jesus � trabalhar!
Prontamente, fielmente, trabalhar!
Em servi-lO, que prazer!
E s� tu, � crente, o poder�s fazer!

2. Para cada crente o Mestre preparou
Um trabalho certo, quando o resgatou;
E o trabalho a que Jesus te chama aqui,
Como vai ser feito, se o n�o for por ti?

3. Pode ser humilde, mas, se for pra Deus,
Teu trabalho � visto l� dos altos c�us!
E o esfor�o nunca pode ser em v�o,
Se tiver de Cristo plena aprova��o!

4. Quantos h� perdidos, sem a salva��o!
Quantos que precisam de consola��o!
Como Cristo os ama, faze-os entender,
Pois s� tu, � crente, o poder�s fazer!