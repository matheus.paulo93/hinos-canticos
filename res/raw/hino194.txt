Hino 194  Ano Novo 
Jo�o Gomes da Rocha

1. Rompe a aurora, vai-se embora
Mais um ano de labor;
N�o temamos, prossigamos,
A lutar com mais ardor.

O ano findo nunca mais veremos;
O ano novo hoje recebemos!
V�, v�, o belo dom que Deus nos d�!

2. Cada dia Cristo, o Guia,
Nos renove o cora��o;
Temos gozo, bom repouso,
Confiando em Sua m�o.

3. Do pecado resgatados,
Pertencemos a Jesus;
Nova vida, santa lida,
Temos n�s por Sua cruz.

4. Hinos santos entoemos
E louvemos ao Senhor!
Vem do arcano mais um ano
Que anuncia Seu favor! 