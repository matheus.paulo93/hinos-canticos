Hino 138  No Lar Celestial 
William Anglin

1. No lar celestial do santo Deus
N�o pode entrar o mal, nem homens r�us.
A morte cessar�, paz perfeita reinar�
E dor n�o haver� nos santos c�us.

2. No glorioso lar dos santos c�us
Os crentes v�o morar junto com Deus;
Felizes estar�o, pois nos c�us h� perfei��o,
E sempre ali dar�o louvor a Deus.

3. No santo lar Jesus vive com Deus,
Na resplendente luz dos altos c�us; 
E Quem foi preparar para os santos um lugar
Um dia h� de voltar dos mesmos c�us.  