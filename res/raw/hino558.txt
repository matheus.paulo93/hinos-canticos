Hino 558  Ao meditar 
Jos� Il�dio Freire

1. Ao meditar, Jesus Senhor,
Na Tua amarga cruz por mim,
Com gratid�o e com louvor
Celebro a Quem me amou assim. 

2. Mas como, � Salvador, contar
O amor que foi � cruz por mim?
N�o poder� ningu�m sondar
A dor de Quem sofreu assim.

3. Pecado meu Te fez sofrer
De Deus o desamparo ali;
Mas mesmo pelo Teu morrer
Pudeste vida dar-me em Ti. 

4. 'T� que contigo v� gozar
O fruto dessa cruz, Senhor,
Que aqui Tu possas sempre achar
Em mim agrado e vero amor.