Hino 135  Cada Cora��o Procura
Henry Maxwell Wright

1. Cada cora��o procura
Onde possa descansar,
Mas descanso verdadeiro
S� Jesus o pode dar.

Cristo sempre e t�o somente,
Cristo, Salvador e Rei!
Meu Amigo, meu Abrigo,
Tudo, tudo nEle achei!

2. Hoje a Ti, Jesus, me entrego,
� bendito Salvador!
Sejas Tu, Jesus, pra sempre
O meu Rei, meu bom Senhor!    