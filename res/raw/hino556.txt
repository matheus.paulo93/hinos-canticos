Hino 556  Nome sem igual 
Edmund Stuart Mc Nair

1. "Jesus", que Nome sem igual!
O Vencedor de todo mal,
Que Nome insigne, triunfal,
Teu Nome, � Jesus Cristo!

Cristo, que morreu na cruz!
Cristo, vivo em santa luz!
Cristo! Salvador Jesus!
Oh, Nome incompar�vel! 

2. Jesus, de Quem, no cora��o,
Os nomes do Teu povo est�o,
Os Teus remidos louvar�o,
Jesus, Teu santo Nome! 

3. N�o pode nossa voz cantar,
Nem nosso cora��o sondar,
A gra�a divinal, sem par,
Que traz, Jesus, Teu Nome!