Hino 356  �s minha rocha
Eduardo Moreira Henriques

1. �s minha Rocha, Senhor da vida;
�s meu Ref�gio, livre serei!
�s minha Rocha, d�s-me guarida,
Eternamente em Ti morarei.

� Redentor, �s Tu minha Rocha,
Amigo meu, meu Guia e meu Rei!
Meu Salvador, �s minha Esperan�a:
Ref�gio em Ti seguro acharei!

2. Que �s minha Rocha nas mil fraquezas,
Que �s minha Rocha na prova eu sei.
�s minha Rocha para as tristezas;
�s minha Rocha; n�o temerei! 

3. �s minha Rocha quando tentado;
�s meu Amigo: n�o mudar�s.
Em Ti me sinto bem amparado;
Fiel e puro me manter�s.

4. Quando as coroas se pulverizam,
�s minha Rocha de salva��o;
Quando os imp�rios se finalizam,
Em Ti confia meu cora��o! 