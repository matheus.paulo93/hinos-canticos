Hino 422  Jesus Satisfaz 
Jos� Il�dio Freire

1. Jesus, �s Tu Quem satisfaz
Aspira��es de vida e luz;
� s� em Ti que encontro paz;
Descanso tenho em Tua cruz!

2. Tua Palavra, � meu Senhor,
� sempre a mesma, e digna �
De cativar o meu amor
E acrisolar a minha f�!

3. O P�o da Vida em Ti achei,
�s meu sustento, � P�o do C�u!
Jamais no mundo eu encontrei
Bom alimento igual ao Teu!

4. Minha alma aspira s� por Ti,
E quer gozar o Teu amor;
Pois muda e passa a vida aqui,
Mas Tu, n�o mudas, meu Senhor!

5. Cristo Jesus, Senhor, �s meu,
Comigo est�s, e Teu eu sou;
E sei que vou gozar no c�u
Com Quem, na cruz, me resgatou!