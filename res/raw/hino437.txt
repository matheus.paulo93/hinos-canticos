Hino 437  Can��o de Amor 
Luiz Soares

1. Eu recebi, dos c�us, um hino
Que me deu meu Salvador.
Jamais t�o doce melodia ouvi,
T�o real can��o de amor! 

Oh, que gozo infindo, sem igual,
Me traz ao cora��o o som de tal can��o!
Oh, que melodia triunfal,
Can��o celestial de amor. 

2. Ao meu Senhor Jesus eu amo,
Pois morreu pra me salvar;
E p�s um hino em mim, no cora��o,
Que h� de sempre ali ficar. 

3. Quando eu chegar ao lar celeste,
Ao Senhor adorarei,
E a melodia linda que me deu,
Para sempre cantarei.