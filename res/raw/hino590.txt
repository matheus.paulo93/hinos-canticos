Hino 590  Medita��o sublime 
Albert Henry Storrie

1. Jesus, Senhor, em Ti pensar
Nos traz do�ura e paz;
Qu�o deleit�vel meditar
No amor que satisfaz!

2. E muito mais, Senhor Jesus,
N�s desejamos ver
Teu rosto na celeste luz,
No c�u Te engrandecer.

3. Que seja para Ti, Senhor,
Suprema devo��o!
�s digno, nosso Salvador,
De toda a adora��o.