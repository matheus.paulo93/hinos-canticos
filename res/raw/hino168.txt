Hino 168  Teu Cantinho 
Robert Hawkey Moreton

1. Manda-nos luzir o Senhor Jesus,
Como quando a vela d� de noite a luz.
Quer que n�s brilhemos como a luz do c�u;
Eu no meu cantinho e tu no teu.

2. Ao redor, ent�o, manda a luz raiar,
Pois que muitas trevas h� que dissipar.
Para reluzirmos Deus nos acendeu;
Eu no meu cantinho e tu no teu. 