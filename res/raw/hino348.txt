Hino 348  Meu Senhor me Guia 
Salom�o Luiz Ginsburg

1. Meu Senhor me guia sempre,
Perto dEle quero estar,
Confiando em meu Amado
Que me ensina a caminhar.
Paz perfeita, gozo infindo,
� fruir a prote��o
De Quem sempre por mim vela
Com bondoso cora��o.

2. Meu Senhor me guia sempre,
Ensinando-me a viver,
Concedendo gra�a e for�a
Para eu mais na f� crescer.
Perecer jamais receio,
Pois quem pode arrebatar
Do Pastor qualquer ovelha
Que Ele sabe e quer guardar?

3. Meu Senhor me guia sempre,
Me corrige com amor;
Vida santa, gl�ria eterna,
Me faculta o Salvador.
E mui breve vem o dia
Em que a plena reden��o,
Corpo e alma transformados,
Os remidos gozar�o. 