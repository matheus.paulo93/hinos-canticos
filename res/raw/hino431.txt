Hino 431  Alegro-me em Jesus 
Stuart Edmund Mc Nair

1. Alegro-me em Jesus, meu Salvador,
Pois fez-Se, sobre a cruz, meu Salvador.
Por mim ali sofreu, por mim ali morreu
E salva��o me deu, meu Salvador.

2. Jesus me revelou o Seu amor
E satisfeito estou com Seu amor.
Minha alma nEle tem o seu presente bem
E sua sorte al�m no Seu amor. 

3. H� sempre gozo e paz com Cristo ali;
E tudo satisfaz com Cristo ali.
N�o haver� mais dor, nem qualquer dissabor,
Mas gozo, paz e amor com Cristo ali.