Hino 404  Jovens Lutadores 
Robert Hawkey Moreton

1. � jovens, acudi ao brilhante pavilh�o
Que Jesus h� desfraldado na na��o!
A todos Cristo quer nas fileiras receber
E mui firmes nos levar o mal a combater!

Vamos com Jesus e marchemos sem temor!
Vamos ao combate, inflamados de valor!
Com coragem vamos todos contra o mal!
Em Jesus teremos nosso General!

2. � jovens, acudi ao divino Vencedor;
Quer juntar-vos todos hoje a Seu redor!
Dispostos a lutar, vinde, pois, sem vacilar;
Vamos prontos, companheiros, vamos a lutar!

3. Quem nesta guerra entrar Sua voz escutar�,
Cristo ent�o vit�ria lhe conceder�!
Saiamos, meus irm�os, invistamos mui fi�is;
Com Jesus conquistaremos imortais laur�is!