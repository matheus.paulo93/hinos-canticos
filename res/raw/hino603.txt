Hino 603  Isto eu sei 
David Glass

1. N�o sei por que o amor foi revelado
De um Ser a Quem os anjos culto d�o!
Por que, qual Bom Pastor, quis procurar-nos
A fim de nos livrar da perdi��o.
Mas isto eu sei: nasceu qual criancinha,
Na pobre manjedoura de Bel�m;
Como homem puro e nobre andou na terra,
O Salvador que rejeitaram com desd�m.

2. N�o sei avaliar o pre�o amargo
Da paz perene que Ele nos deixou,
Nem compreendo como, quebrantado,
Na rude cruz, Seu cora��o ficou.
Mas isto eu sei: que alenta as almas tristes
E purifica o mais vil pecador,
Ao sobrecarregado traz al�vio,
Pois sempre permanece o mesmo Salvador. 

3. N�o sei se os povos h�o de receb�-lO,
Ou quando reger� seus cora��es,
Como h� de preencher perfeitamente,
De s�bio e simples, as aspira��es.
Mas isto eu sei: ver�o a Sua gl�ria,
Do sofrimento o fruto h� de brotar
E, em dia alegre, a luz do c�u radiante
O Salvador trar� quando Ele aqui voltar. 

4. N�o sei prever o que n�s sentiremos
Maravilhados ante o resplendor
DAquele Ser a Quem n�s tanto amamos
E nos apraz ouvir a voz de amor,
Mas isto eu sei: que em jubiloso canto
Os c�us e a terra juntos louvar�o
Ao contemplar em majestade e gl�ria
O Salvador Real a Quem adorar�o.