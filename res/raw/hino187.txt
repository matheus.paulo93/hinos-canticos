Hino 187  Identifica��o 
Luiz Soares

1. Senhor Jesus, desejo alegremente
Ao Teu mandado, agora, obedecer;
Com f� sincera, humilde e reverente,
Venho ao batismo aqui me submeter.
Testemunhar eu quero, neste ensejo,
Que a vida nova tenho, agora, em Ti,
E mais e mais, de cora��o, desejo
Glorificar-Te em meu viver aqui.

2. � meu Salvador, que grande dor sofreste
Quando, na cruz, tomaste o meu lugar 
E da Justi�a o golpe recebeste,
Ali morrendo para me salvar!
Mas no sepulcro n�o permaneceste,
Ressuscitaste e n�o mais morrer�s,
Pecado e morte e Satan�s venceste,
E entronizado junto ao Pai est�s.

3. Pois eu tamb�m morri para o pecado,
Para contigo, em vida nova, andar;
E com prazer, � Cristo, meu amado,
Venho este fato agora declarar;
Pois des�o �s �guas com supremo agrado,
Para, em figura, ao mundo proclamar
Que fui contigo morto e sepultado,
E ressurreto vou contigo andar. 