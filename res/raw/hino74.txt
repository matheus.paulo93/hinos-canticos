Hino 074  Jesus chama
Stuart Edmund Mc Nair

1. H� Quem espere ansioso
Hoje de Deus o perd�o, 
Quem queira obter sem demora
Uma real salva��o?
Podes ach�-la com Cristo,
Que sobre a cruz j� morreu;
Podes ter nEle descanso,
Vida e prazeres no c�u.

Hoje te chama Jesus, Jesus, Jesus;
Vem a Jesus, j�, contrito;
NEle ser�s tu bendito,
Pois hoje te chama Jesus,
Querendo te perdoar.

2. B�n��o divina oferece
Nosso bendito Senhor,
Felicidade inef�vel,
Parte em Seu rico favor.
Pede, pois ser-te-� dada;
Toma, sem nada pagar:
Goza, por f�, mesmo agora
Da Sua gra�a sem par.

3. S� pela f� no Seu sangue
� que podemos obter
Esse perd�o desejado,
Vida, paz, gozo e prazer.
Sempre em Jesus confiados,
Ele nos d� salva��o;
Sendo por Ele amparados
Nos guardar� Sua m�o. 