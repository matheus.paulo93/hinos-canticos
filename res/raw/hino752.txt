Hino 752 Andei sem luz
Kenneth Leslie Cox

Outrora andei sem luz, sem f�,
Em ignor�ncia e dor,
At� que Deus, amando os r�us,
Mostrou-me Seu amor.
Conhe�o, enfim, que foi por mim
Que o Salvador morreu;
E agora vou, pois salvo estou,
Alegre para o c�u.  