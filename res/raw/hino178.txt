Hino 178  Fi�is ao Senhor 
Luiz Soares

1. Seguindo a Jesus, andando na luz,
Oh, sejam fi�is ao Senhor;
Servindo em amor, com todo o vigor,
Sim, sejam fi�is ao Senhor.

Fi�is sejam sempre ao Senhor.
Ele � poderoso, jamais falhar�;
Com Seu forte bra�o Ele os defender�.
Sim, sejam fi�is ao Senhor.

2. Conflitos vir�o, tentados ser�o,
Mas sejam fi�is ao Senhor. 
Ele h� de ajudar, vit�ria h� de dar,
Sim, sejam fi�is ao Senhor.

3. Voc�s vencer�o, coroas ter�o,
Se forem fi�is ao Senhor,
E as receber�o com satisfa��o
Das m�os do fiel Salvador. 