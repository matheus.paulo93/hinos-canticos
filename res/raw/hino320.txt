Hino 320  Compaix�o 
William Anglin

1. � tarde veio a multid�o
Do povo fraco e sofredor,
Chegando com sua afli��o
Aos santos p�s do Salvador.

2. Quem veio com doen�a e dor
Voltou curado para o lar,
Com gratid�o ao Salvador
Que a todos p�de aliviar.

3. Qu�o bom saber que Tua m�o,
Igual virtude ainda tem, 
E que hoje podes, como ent�o,
Proporcionar-nos todo o bem!

4. Pois n�s, tamb�m, Jesus Senhor,
Como eles, somos sem poder:
Contamos com o mesmo amor
Agora para nos valer.

5. � Cristo, vimos hoje a Ti,
Rogando a mesma compaix�o;
Oh, sobre n�s, os Teus aqui,
Estende a Tua santa m�o! 	