Hino 127  Oh, Vinde a Mim 
Albert Henry Storrie

1. "Oh, vinde a Mim, descanso vos darei.
V�s, oprimidos, vinde, recebei
A verdadeira paz que n�o tem fim",
Cristo Jesus � Quem vos fala assim.

"Oh, vinde a Mim! Oh, vinde a Mim!
Oh, vinde a Mim! Eu vos aliviarei,
Vos aliviarei, vos aliviarei".

2. Vinde a Jesus: tereis consola��o.
Outro n�o h� que fez expia��o;
� s� Jesus capaz de vos valer;
O Seu perd�o, pois, vinde receber.

3. Vinde a Jesus: felizes v�s sereis;
B�n��os celestes v�s alcan�areis.
Por Seu amor e pelo Seu poder,
Aos pobres Cristo faz enriquecer. 