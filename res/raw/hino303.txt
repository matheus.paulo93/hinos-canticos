Hino 303  Paz Com Deus 
Stuart Edmund Mc Nair

1. Paz com Deus! Busquei ganh�-la
Com o mais real fervor,
Sem, contudo, achar descanso,
Nem sossego interior. 

Oh, que paz Jesus me d�!
Paz que outrora eu ignorei!
Tudo novo se tornou,
Desde que esta paz achei.

2. Cheio estava de temores,
Sem a causa descobrir;
Ora em calma, ora em luta,
Ignorando o meu porvir.

3. Afinal, em desespero,
Disse: "Sem vigor estou!"
E do c�u ouvi resposta:
"Cristo tudo consumou".

4. Livre assim das minhas obras,
Vendo aquela de Jesus,
Soube como a paz foi feita
Por Seu sangue sobre a cruz.

5. Esta paz n�o muda nunca;
� constante o seu valor;
E por Deus � garantida
Ao que a Jesus Cristo for.