Hino 004  Alegre Verdade 
Richard Holden

1. Alegre verdade! qu�o bom � ouvir
Que ao Deus de bondade aprouve em abrir
� gl�ria o caminho ao vil pecador.
Vit�ria da gra�a! Vit�ria do amor!

2. Foi ato espont�neo da parte de Deus
Seu Filho enviar-nos da gl�ria dos c�us
E, por Sua morte, o campo aplanar
A fim de tal gra�a poder operar! 

3. L� estava o pecado, t�o amea�ador,
A todos vedando as b�n��os do amor,
Clamando � justi�a que, sem perdoar,
A culpa devia nos r�us castigar.

4. Mas Deus ao Seu Filho ao mundo enviou
E o fim desejado por Ele alcan�ou,
Pois Cristo a Si mesmo � morte Se deu
E o grande empecilho assim removeu.

5. Se cremos em Cristo, Deus pode ficar
Justo, inda no ato de justificar,
Pois fica a justi�a agora a favor
Daquele que � crente em Cristo, o Senhor.

6. Aos homens Deus chama, pedindo aten��o
� voz que proclama o pleno perd�o,
E o d� mesmo �quele que mais culpas tem
Quando, arrependido, ao Salvador vem. 