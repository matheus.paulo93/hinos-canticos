Hino 318  Dai a Deus o Louvor
An�nimo

1. Dai a Deus o louvor sem medida!
Seu amor eternal nos inunda;
Sua gra�a em sinais � fecunda:
Seus louvores, humildes, cantai!
Ao Senhor dos senhores dai gl�ria;
Rei dos reis que rival n�o conhece;
Todo reino do mundo perece,
Mas Seu reino n�o finda jamais!

2. As na��es viu em v�cios imersas
E sentiu compaix�o no Seu seio;
De prod�gios, de gra�a est� cheio:
Seus louvores, humildes, cantai!
A Seu povo Ele est� conduzindo 
Para a P�tria feliz, prometida;
Em Jesus eles t�m sua vida,
Seu poder, seu louvor e seu bem.

3. A Seu Filho enviou pra salvar-nos,
Pela cruz, do poder do pecado;
Pra faz�-lo, tomou nosso estado:
Seus louvores, humildes, cantai!
Neste mundo, do mal nos defende
E ao celeste descanso nos guia,
Para termos com Ele alegria,
Alegria que n�o findar�. 	