Hino 421  Sem Meu Senhor Amado 
Albert Henry Storrie

1. Sem meu Senhor amado
N�o posso caminhar,
Pois fui por Ele salvo
Por gra�a singular;
Seu sacrif�cio e morte,
Que em meu lugar sofreu,
Ser�o meu tema eterno,
Pois sou pra sempre Seu!

2. Sem meu Senhor amado
Sou fraco e sem vigor,
Mas, Quem me fortalece
� Cristo, em Seu amor.
No Seu poder confio
E a gra�a que Ele d�
Me basta na fraqueza,
Que em for�a mudar�.

3. Com meu Senhor eu sigo
Enquanto os anos v�o
E, embora longa seja
A peregrina��o,
Cristo est� sempre perto
Para meus p�s guiar
No Seu caminho reto
At� ao C�u chegar.