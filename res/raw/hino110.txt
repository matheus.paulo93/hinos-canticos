Hino 110  Vem, vem
Willian Anglin

1. Vem, vem ao Salvador, tu que distante est�s;
Jesus, o Mediador, com Deus fez nossa paz.
Vem, vem, � pecador, no teu pecado e mal;
Jesus, o Salvador, d� b�n��o sem igual.
Vem, vem, vem!

2. Vem, vem ao Salvador; eis sobre a dura cruz
Morreu teu Redentor, a Vida, Amor e Luz;
Fazendo expia��o, e pela morte, ali
Obteve a reden��o, que em gra�a oferta a ti.
Vem, vem, vem!

3. Vem, vem ao Salvador; recebe Seu perd�o,
Pois Ele tem amor e terna compaix�o.
Tu, oprimido, vem! Tu, que cansado est�s!
Jesus agora tem pra ti perd�o e paz.
Vem, vem, vem! 		