Hino 047  Vida por um olhar
Ant�nio Ferreira de Campos

1. Ter�s vida em olhar pra Jesus, Salvador;
Ele diz: "Vida eterna Eu te dou";
Pois ent�o, pecador, considera esse amor,
V� Jesus que na cruz expirou.

V�, v�, viver�s!
Ter�s vida em olhar pra Jesus, Salvador;
Ele diz: "Vida em Mim achar�s!" 

2. Ter�s vida em olhar pra Jesus, Salvador;
Sangue Seu derramado Ele tem;
Paga est� nossa falta; n�o haja temor;
Por olhar, pecador, vida vem.

3. Ter�s vida em olhar pra Jesus, Salvador;
N�o h� choro, remorso, nem dor
Que consiga remir a qualquer pecador;
S� o sangue do bom Redentor.

4. Ter�s vida em olhar pra Jesus, Salvador;
Ele tudo por ti j� sofreu.
Deus estende o convite ao maior transgressor;
V� Jesus que por ti padeceu!

5. Ter�s vida em olhar pra Jesus, Salvador;
Ele diz: "Vida eterna Eu te dou";
Nunca perecer�s, crendo em Cristo, o Senhor;
Seguran�a em Jesus gozar�s.