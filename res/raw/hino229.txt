Hino 229  Jesus, Fiel Amigo 
Stuart Edmund Mc Nair

1. Jesus, fiel Amigo, contigo salvo estou,
Seguro do perigo, Teu bra�o me livrou.
Nem quero mais prazeres dos tais que o mundo d�;
Te quero a Ti e queres contigo ter-me l�.

2. Quem me encher� de gozo sen�o o Teu amor?
Ningu�m � t�o bondoso como �s, meu Salvador.
E tudo me forneces, pois t�o am�vel �s;
De mim Te compadeces, o que � mister prov�s.

3. Por que sentir tristeza? Teu Nome sem igual
� minha fortaleza, ref�gio meu do mal.
Tomei a cruz e sigo a Ti, meu Redentor,
E sempre ir� comigo o Teu real favor.

4. � gl�ria v�, mundana, em v�o me vens chamar!
H� gl�ria sobre-humana que em ti n�o posso achar;
Pois j� que a gl�ria vejo de Cristo, meu Senhor,
� todo o meu desejo viver no Seu amor. 
