Hino 270  Por Mim 
Stuart Edmund Mc Nair

1. Senhor Jesus, tomaste meu lugar,
Qual santo sacrif�cio sobre o altar;
Vieste a Tua pr�pria vida dar,
Senhor, por mim, por mim!

2. Teu sangue, � Cristo, deu-me salva��o;
Teu desamparo me ganhou perd�o,
Nessa hora em que sofreste a maldi��o,
Senhor, por mim, por mim!

3. Tal como foste, o mesmo �s Tu, Senhor,
Pois imut�vel � Teu santo amor.
Em gl�ria vives como Salvador,
Senhor, por mim, por mim! 

4. Em Ti descanso com f� cordial,
Em Ti, meu Defensor do temporal,
At� que, em breve, voltes, afinal,
Senhor, por mim, por mim!      