Hino 416  Oh! Quanto Desejo
Autor Desconhecido

1. Oh! Quanto desejo servir-Te, Senhor,
Com voz mais alegre cantar Teu louvor!
Oh! Quanto desejo exaltar-Te sem fim,
Porque no madeiro morreste por mim!

Desejo, sim, sempre Teu Nome entoar,
Na Tua bondade e amor confiar.

2. Oh! Quanto desejo contigo trilhar
A senda divina, e Te confessar
Em tudo meu Mestre, meu Guia e Senhor,
Provando-Te sempre meu Deus Salvador!

3. Tamb�m eu desejo aos vizinhos mostrar
As Tuas bondades e gra�a sem par,
Assim confessando o divino poder
Que Teu Evangelho no mundo h� de ter.