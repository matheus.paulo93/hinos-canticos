Hino 123  Rocha Eterna 
Henry Maxwell Wright

1. Rocha eterna, meu Jesus,
Que por mim na amarga cruz
Foi ferido em meu lugar,
Morto para me salvar;
NEle quero me esconder,
Pois me poder� valer.

2. Minhas obras, bem o sei,
Nada valem ante a lei;
Se chorasse sem cessar,
Trabalhasse sem cansar,
Meu af� seria em v�o:
S� em Cristo h� salva��o.

3. Nada trago a Ti, Senhor;
Conto s� com Teu amor.
De perd�o indigno sou
E sem Ti perdido estou,
Mas Teu sangue, � Salvador,
Lava o pobre pecador.

4. Quando a morte me chamar
E contigo eu for morar,
Rocha eterna, meu Jesus,
Que por mim na amarga cruz
Padeceste em meu lugar,
Quero sempre a Ti louvar. 